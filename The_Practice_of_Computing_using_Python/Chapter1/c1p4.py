# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 19:20:52 2017

The Practice of Computing Using Python
Second Edition
Programming Projects
Chapter: 1
Problem: 4 "Population estimation"

@author: Jacques Troussard
"""
# estimated population of USA from http://www.census.gov 3/22/2017
# rates of birth, death and immagration given by text p80

# current population
US_POPULATION = 324738069
CURRENT_YEAR = 2017

# convert given rates into per year
BPY = 31536000/7 
DPY = 31536000/13
IPY = 31536000/35

# take user input in years
input_years = int(input("Enter number of years to predict: "))

# perform necessary calculations
year = CURRENT_YEAR + input_years

population = US_POPULATION + (input_years * (BPY+IPY)) - (input_years * (DPY))

# print statement estimating the population
print ("In the year {} the estimated population will be {}.".format(year, population))