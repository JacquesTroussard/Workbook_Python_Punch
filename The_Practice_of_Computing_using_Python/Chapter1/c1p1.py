# -*- coding: utf-8 -*-
"""
Created on Wed Mar 15 19:47:34 2017

The Practice of Computing Using Python
Second Edition
Programming Projects
Chapter: 1
Problem: 1

@author: Jacques Troussard
"""
# To find the volume of an area, if another volume were emptied into it, the
# emptying volume is to be divided by the area, returning the height that 
# would result in the 'filling' of the area.

#approimatedly (in km^2)
usa_surface = 8080464
#approimatedly (in km^3)
grlakes_vol = 22810

height = grlakes_vol/usa_surface

print ("Spread evenly across the 48 contiguious states, the volume of the Great Lakes would submerge the area in approximately {} km of water.".format(height))