# -*- coding: utf-8 -*-
"""
Created on Wed Mar 15 19:47:34 2017

The Practice of Computing Using Python
Second Edition
Programming Projects
Chapter: 1
Problem: 2

@author: Jacques Troussard
"""
# To find the distance an object has traveled, the object's velocity is 
# multiplied by the time traveled. Add this figure to the distance already
# traveled and the total distance will be found.

#contants
HOURS_IN_DAY = 24
KM_IN_MILE = 1.609344
MILES_IN_AU = 92955887.6
METER_IN_MILE = 1609.34
C = 299792458 #Speed of light in meters per second

#distance from sun on 09/25/2009 (in miles)
base_distance = 16637000000
#velocity (mph)
v = 38241

#user input
input_days = int(input("Enter the number of additional days traveled for Voyager: "))

#distance in miles
rnt_miles = (input_days * 24 * v) + base_distance
#distance in kilometers
rnt_km = rnt_miles * KM_IN_MILE
#distance in astronomical units
rnt_au = rnt_miles / MILES_IN_AU
#radio transmission round-trip time
rnt_radio_time = (rnt_miles / (((C / 1609.34) * 60) * 60) * 2)

print ("Total distance in miles: {}".format(rnt_miles))
print ("Total distance in kilometer: {}".format(rnt_km))
print ("Total distance in astronomical units: {}".format(rnt_au))
print ("Total time for round-trip radio transmission: {}".format(rnt_radio_time))