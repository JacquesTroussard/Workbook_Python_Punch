# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 18:55:14 2017

The Practice of Computing Using Python
Second Edition
Programming Projects
Chapter: 1
Problem: 3 "Oil conversions and calculations"

@author: Jacques Troussard
"""

input_float = float(input("Enter gallons of gasoline: "))

in_liters = input_float * 3.78541
print("Number of liters: {}".format(in_liters))

in_barrels = input_float / 19.5
print("Number of barrels of oil required: {}".format(in_barrels))

in_co2 = input_float * 20
print("Number of pounds of CO2 produced when burned: {}".format(in_co2))

in_ethanolbtu = (input_float * 115000) / 75700
print("Equivelent energy amount in ethanol gallons: {}".format(in_ethanolbtu))

in_priceusd = (input_float * 2.29)
print("Price in U.S. Dollars: {}".format(in_priceusd))