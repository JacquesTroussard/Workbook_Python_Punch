# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 20:50:42 2017

The Practice of Computing Using Python
Second Edition
Programming Projects
Chapter: 2
Problem: 1 "What is the invention of Chess worth"

@author: Jacques Troussard
"""

# number of squares on the board
board = 8

# number of grains
grains = 1


def countGrains(squares):
    grain = 1
    if (squares == 0):
        return 0
    else:
        return grain + 2*countGrains(squares-1) 

total_grains = countGrains(64)

print("The ruler had to pay the inventor {} pieces of grain".format(total_grains))
print("The approximate weight of the wheat was {}mg".format(total_grains * 50))

print("\nThe third part of this question asks for a description of how much "+
      "the wheat would cover, in terms of a region. I protest, as wheat is " +
      "agricultural item and can vary in volume. Mostly I'm to lazy to " + 
      "look it up. Also seems silly to repeat this exercise seeing as it " +
      "just done last chapter.")



    
