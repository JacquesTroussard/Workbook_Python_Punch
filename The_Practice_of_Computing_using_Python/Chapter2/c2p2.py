"""
Created on Thurs Mar 22 23:00:42 2017

The Practice of Computing Using Python
Second Edition
Programming Projects
Chapter: 2
Problem: 2 "How thick does paper folding get?"

@author: Jacques Troussard
"""
from fractions import Fraction as fr

# const thickness of unfolded piece of paper
thickness = fr(1,200);

i_folds = int(input("Enter the number of times to fold the piece of paper: "))
ht_in_meters = (2 ** i_folds) * float(thickness) / 100

print ("After folding the paper {} times it's height would be {} meters.".format(i_folds, ht_in_meters))
