"""
Created on Thurs Mar 22 23:49:00 2017

The Practice of Computing Using Python
Second Edition
Programming Projects
Chapter: 2
Problem: 4 "Weird multiplication"

@author: Jacques Troussard
"""
a = int(input("Enter the first value:  "))
b = int(input("Now the second value: "))
result = 0

while (b > 0):
    if(b % 2 != 0):
        result = a + result
    a = a * 2
    b = int(b / 2)
print(result)
